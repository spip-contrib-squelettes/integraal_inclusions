<?php

return [
	'bouton_voir_plus' => 'Voir plus',
	'info_1_objet' => 'Un contenu',
	'info_aucun_objet' => 'Aucun contenu',
	'info_nb_objet' => '@nb@ contenus',
];
