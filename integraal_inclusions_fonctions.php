<?php

// Si le plugin Polyhiérarchie n'est PAS actif, on redéfinit plusieurs critères à vide, juste pour permettre de les utiliser avec ou sans le plugin
if (!test_plugin_actif('polyhier')) {
	function critere_enfants($idb, &$boucles, $crit) {}
	function critere_enfants_directs_dist($idb, &$boucles, $crit) {}
	function critere_enfants_indirects_dist($idb, &$boucles, $crit) {}
	function critere_parents($idb, &$boucles, $crit) {}
	function critere_parent($idb, &$boucles, $crit) {}
	function critere_parents_directs_dist($idb, &$boucles, $crit) {}
	function critere_parents_indirects_dist($idb, &$boucles, $crit) {}
	function critere_branche($idb, &$boucles, $crit) {}
	function critere_branche_principale_dist($idb, &$boucles, $crit) {}
	function critere_branche_directe_dist($idb, &$boucles, $crit) {}
	function critere_branche_indirecte_dist($idb, &$boucles, $crit) {}
	function critere_branche_complete_dist($idb, &$boucles, $crit) {}
}
